<?php require_once("includes/config.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>VideoTube</title>
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">

    <script src="assets/bootstrap/js/popper.min.js"></script>
    <script src="assets/bootstrap/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/commonActions.js"></script>
</head>
<body>

    <div id="pageContainer">
        
        <div id="mastHeadContainer">
            <button class="navShowHide">
                <img src="assets/images/icons/menu.png" alt="">
            </button>

            <a class="logoContainer" href="index.php">
                <img src="assets/images/icons/VideoTubeLogo.png" title="logo" alt="Site logo">
            </a>

            <div class="searchBarContainer">
                <form action="search.php" method="get">
                    <input type="text" name="term" class="searchBar" placeholde="Search...">
                    <button class="searchButton">
                        <img src="assets/images/icons/search.png" alt="">
                    </button>
                </form>
            </div>

            <div class="rightIcons">
                <a href="upload.php">
                    <img class="upload" src="assets/images/icons/upload.png">
                </a>
                <a href="#">
                    <img class="profile" src="assets/images/profilePictures/default.png">
                </a>
            </div>

        </div>

        <div id="sideNavContainer" style="display: none;">
        
        </div>

        <div id="mainSectionContainer">

            <div id="mainContentContainer">