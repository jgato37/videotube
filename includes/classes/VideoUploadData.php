<?php

    class VideoUploadData{

        public $videoDataArray, $title, $description, $privacy, $category, $uploadedBy;

        public function __construct($videoDataArray, $title, $description, $privacy, $category, $uploadedBy){

            $this->videoDataArray = $videoDataArray;
            $this->title = $title;
            $this->description = $description;
            $this->privacy = $privacy;
            $this->category = $category;
            $this->uploadedBy = $uploadedBy;
        }

        // We can use these functions and more others to retrieve the class level variables. However,
        // for the sake of simplicity, we want to make the field(class level) variables, public, so that,
        // they can be accessed from outside the class

        // public function getTitle(){
        //     return $this->title;
        // }

        // public function getDescription(){
        //     return $this->description;
        // }

    }

?>